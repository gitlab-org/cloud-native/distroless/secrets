package main_test

import (
	"fmt"
	"os"
	"regexp"
	"testing"

	"github.com/lucasjones/reggen"
	"github.com/pelletier/go-toml"
	"github.com/stretchr/testify/require"
)

type TOMLRule struct {
	ID    string
	Regex string
}

type GitleaksTOML struct {
	Title string
	Rules []TOMLRule
}

var ExcludedRules = map[string]struct{}{
	"PKCS8 private key":                             {},
	"RSA private key":                               {},
	"SSH private key":                               {},
	"PGP private key":                               {},
	"systemd-machine-id":                            {},
	"Github Personal Access Token":                  {},
	"Github OAuth Access Token":                     {},
	"SSH (DSA) private key":                         {},
	"SSH (EC) private key":                          {},
	"Github App Token":                              {},
	"Github Refresh Token":                          {},
	"Shopify shared secret":                         {},
	"Shopify access token":                          {},
	"Shopify custom app access token":               {},
	"Shopify private app access token":              {},
	"Slack token":                                   {},
	"Stripe":                                        {},
	"PyPI upload token":                             {},
	"Google (GCP) Service-account":                  {},
	"GCP API key":                                   {},
	"GCP OAuth client secret":                       {},
	"Password in URL":                               {},
	"Heroku API Key":                                {},
	"Slack Webhook":                                 {},
	"Twilio API Key":                                {},
	"Age secret key":                                {},
	"Facebook token":                                {},
	"Twitter token":                                 {},
	"Adobe Client ID (Oauth Web)":                   {},
	"Adobe Client Secret":                           {},
	"Alibaba AccessKey ID":                          {},
	"Alibaba Secret Key":                            {},
	"Asana Client ID":                               {},
	"Asana Client Secret":                           {},
	"Atlassian API token":                           {},
	"Bitbucket client ID":                           {},
	"Bitbucket client secret":                       {},
	"Beamer API token":                              {},
	"Clojars API token":                             {},
	"Contentful delivery API token":                 {},
	"Contentful preview API token":                  {},
	"Databricks API token":                          {},
	"digitalocean-access-token":                     {},
	"digitalocean-pat":                              {},
	"digitalocean-refresh-token":                    {},
	"Discord API key":                               {},
	"Discord client ID":                             {},
	"Discord client secret":                         {},
	"Doppler API token":                             {},
	"Dropbox API secret/key":                        {},
	"Dropbox short lived API token":                 {},
	"Dropbox long lived API token":                  {},
	"Duffel API token":                              {},
	"Dynatrace API token":                           {},
	"EasyPost API token":                            {},
	"EasyPost test API token":                       {},
	"Fastly API token":                              {},
	"Finicity client secret":                        {},
	"Finicity API token":                            {},
	"Flutterwave public key":                        {},
	"Flutterwave secret key":                        {},
	"Flutterwave encrypted key":                     {},
	"Frame.io API token":                            {},
	"GoCardless API token":                          {},
	"Grafana API token":                             {},
	"Hashicorp Terraform user/org API token":        {},
	"Hashicorp Vault batch token":                   {},
	"Hubspot API token":                             {},
	"Intercom API token":                            {},
	"Intercom client secret/ID":                     {},
	"Linear API token":                              {},
	"Linear client secret/ID":                       {},
	"Lob API Key":                                   {},
	"Lob Publishable API Key":                       {},
	"Mailchimp API key":                             {},
	"Mailgun private API token":                     {},
	"Mailgun public validation key":                 {},
	"Mailgun webhook signing key":                   {},
	"Mapbox API token":                              {},
	"messagebird-api-token":                         {},
	"MessageBird API client ID":                     {},
	"New Relic user API Key":                        {},
	"New Relic user API ID":                         {},
	"New Relic ingest browser API token":            {},
	"npm access token":                              {},
	"Planetscale password":                          {},
	"Planetscale API token":                         {},
	"Postman API token":                             {},
	"Pulumi API token":                              {},
	"Rubygem API token":                             {},
	"Segment Public API token":                      {},
	"Sendgrid API token":                            {},
	"Sendinblue API token":                          {},
	"Sendinblue SMTP token":                         {},
	"Shippo API token":                              {},
	"Linkedin Client secret":                        {},
	"Linkedin Client ID":                            {},
	"Twitch API token":                              {},
	"Typeform API token":                            {},
	"Meta access token":                             {},
	"Oculus access token":                           {},
	"Instagram access token":                        {},
	"Yandex.Cloud IAM Cookie v1 - 1":                {},
	"Yandex.Cloud IAM Cookie v1 - 2":                {},
	"Yandex.Cloud IAM Cookie v1 - 3":                {},
	"Yandex.Cloud AWS API compatible Access Secret": {},
	"Tailscale key":                                 {},
}

func TestGitleaksTOMLRegexp(t *testing.T) {
	tcs := []struct {
		name        string
		pattern     string
		shouldMatch bool
	}{
		{
			name:        "Token within text",
			pattern:     "SOMETEXT%sSOMETEXT",
			shouldMatch: false,
		},
		{
			name:        "Token directly after text",
			pattern:     "SOMETEXT%s",
			shouldMatch: false,
		},
		{
			name:        "Token directly before text",
			pattern:     "%sSOMETEXT",
			shouldMatch: false,
		},
		{
			name:        "Token in quotes",
			pattern:     `password="%s"`,
			shouldMatch: true,
		},
		{
			name:        "Token within ()",
			pattern:     "this is the token(%s)",
			shouldMatch: true,
		},
		{
			name:        "Token by itself",
			pattern:     "%s",
			shouldMatch: true,
		},
	}

	gitleaks := parseGitleaksTOML(t)
	for _, rule := range gitleaks.Rules {
		if _, exists := ExcludedRules[rule.ID]; exists {
			continue
		}
		t.Run(rule.ID, func(t *testing.T) {
			r := regexp.MustCompile(rule.Regex)

			// generate a matching string
			token, err := reggen.Generate(rule.Regex, 100)
			require.NoError(t, err)

			// We're currently using the _same regex_ for random token generation
			// as we use for pattern matching. This presents a problem, since sometimes
			// our regex contains patterns that we want to match against, but not
			// necessarily generate tokens containing these characters.
			//
			// We use a capture group in the regex to allow us to split the entire regex
			// into two groups:
			//
			//   1. The capture group defines the portion of the regex to be used for random password generation.
			//   2. The entire regex is used for matching whether a password is present.
			//
			// See https://gitlab.com/gitlab-org/gitlab/-/issues/467617#note_1959073247 for more details.
			capturedMatch := r.FindStringSubmatch(token)
			// Not all of our regexes contain capture groups. We only manipulate the token if the
			// capture group is present.
			if capturedMatch != nil && len(capturedMatch) > 1 {
				token = capturedMatch[1]
			}

			for _, tc := range tcs {
				t.Run(tc.name, func(t *testing.T) {
					stringToMatch := fmt.Sprintf(tc.pattern, token)

					match := r.Match([]byte(stringToMatch))

					message := "Expected pattern %q to match %q, but it did not. Pattern ID: %q."
					if !tc.shouldMatch {
						message = "Expected pattern %q not to match %q, but it did. Pattern ID: %q"
					}

					require.Equal(t, match, tc.shouldMatch, message, rule.Regex, stringToMatch, rule.ID)
				})
			}
		})
	}
}

// test to demonstrate bugfix for https://gitlab.com/gitlab-org/gitlab/-/issues/458626
func TestBug458626(t *testing.T) {
	brokenRules := map[string]string{
		"gitlab_personal_access_token":     "glpat-nQlCf8gbL_WW1ZRx5Ie-",
		"gitlab_pipeline_trigger_token":    "glptt-mWxQWyoIE5uV634H9l5Qo6vv6GB9QDARKzK3qr7-",
		"gitlab_runner_registration_token": "GR1348941qJztp7DlEpwdwQsWoBc-",
		"gitlab_runner_auth_token":         "glrt-A1GNXjVZm_tao2A9jzD-",
		"gitlab_feed_token":                "feed_token=zTbr5RMN_ZlMLMlfKu1-",
		"gitlab_oauth_app_secret":          "gloas-qSicdf79OoHIaWzYlcEHsTaMBLGg5RMs8S87Jppa0PFaO03NG953Alf9hY6uWP--",
		"gitlab_feed_token_v2":             "glft-dq9FtEJnOzCBf7xhHeL-",
		"gitlab_kubernetes_agent_token":    "glagent-qkTwYDc_e7tUqteJhcQP3edq0vxT6n2HrPR1rtCNuHnDqxU0b-",
		"gitlab_incoming_email_token":      "glimt-vkOi--UPU6LTxe7FvsKur3zf-",
		"gitlab_deploy_token":              "gldt-7zgDpMtmgOQRb3A5loS-",
		"gitlab_scim_oauth_token":          "glsoat-RpX4olfATL4jNshlRSD-",
		"gitlab_ci_build_token":            "glcbt-5zs1h_MBtaIN9Qa1Wux-HIRD8-",
		// the rule corresponding to `Ionic API token` has a regex containing `(?i)[a-z0-9]`, which
		// "considers all case variations of letters, including Unicode case folding".
		// This is why the `ſ` character at the end of the following string is matched, because
		// `ſ` is "Latin small letter long s", and when case insensitivity is enabled via the `(?i)`
		// flag, `ſ` is considered equivalent to s.
		"Ionic API token": "ion_YuWqjt8XXzyKiTBV7jrEyODKWVtWup64DsHbD2PSpſ",
	}

	gitleaks := parseGitleaksTOML(t)

	for _, rule := range gitleaks.Rules {
		if brokenRules[rule.ID] == "" {
			continue
		}

		equalMatch := fmt.Sprintf(`password="%s"`, brokenRules[rule.ID])
		r := regexp.MustCompile(rule.Regex)
		match := r.Match([]byte(equalMatch))
		require.True(t, match, "Expected pattern %q to match %q, but it did not. Pattern ID: %q.", rule.Regex, equalMatch, rule.ID)
	}
}

func TestAWSAccessToken(t *testing.T) {
	gitleaks := parseGitleaksTOML(t)
	rule := findRule("AWS", gitleaks.Rules)

	r := regexp.MustCompile(rule.Regex)
	fakeAwsToken := "AKIA0000000000000000"

	tests := []struct {
		name        string
		testString  string
		shouldMatch bool
	}{
		{
			name:        "Token only",
			testString:  fakeAwsToken,
			shouldMatch: true,
		},
		{
			name:        "Token within text",
			testString:  fmt.Sprintf("sometext%ssometext", fakeAwsToken),
			shouldMatch: false,
		},
		{
			name:        "Token within ()",
			testString:  fmt.Sprintf("this is the token(%s)", fakeAwsToken),
			shouldMatch: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, r.Match([]byte(tt.testString)), tt.shouldMatch)
		})
	}
}

func findRule(ruleID string, rules []TOMLRule) TOMLRule {
	keyRules := make(map[string]TOMLRule, len(rules))

	for _, r := range rules {
		keyRules[r.ID] = r
	}

	return keyRules[ruleID]
}

func parseGitleaksTOML(t *testing.T) *GitleaksTOML {
	cfgReader, err := os.Open("./gitleaks.toml")
	require.NoError(t, err)

	gitleaks := &GitleaksTOML{}

	toml.NewDecoder(cfgReader).Strict(true).Decode(gitleaks)
	return gitleaks
}

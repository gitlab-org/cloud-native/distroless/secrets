require 'toml-rb'

desc "Output toml file with gitlab_blocking tagged rules. This should be done any time a `gitlab_blocking` tagged rule is altered"
task :write_gitlab_blocking do
  write_toml_based_on_tag("gitlab_blocking")
  puts "Check the output.toml file."
  puts "Then `cp output.toml /path/to/gdk/gitlab/gems/gitlab-secret_detection/lib/gitleaks.toml` to start a merge request to update the secret push protection gem."
  puts "Do not check the output.toml file in to the secrets repo."
end

def write_toml_based_on_tag(tag_to_select)
  rules_data = TomlRB.load_file('gitleaks.toml')

  # This is a gitleaks specific config, and removing it makes the diff cleaner
  # but maybe we shouldn't get rid of it because other consumers of this might want it?
  rules_data.delete("allowlist")

  # Filter rules based on tags
  rules_data['rules'].reject! do |rule|
    tags = rule["tags"]
    tags.nil? || !tags.include?(tag_to_select)
  end

  toml_string = TomlRB.dump(rules_data)
  toml_string.prepend("# See the README.md of the secrets analyzer for more info: https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/README.md#syncing-tags\n")
  toml_string.prepend("# This file is auto-generated, do not edit.\n")
  File.write('output.toml', toml_string)
end

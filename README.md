# secrets analyzer

secrets analyzer performs Secret Detection scanning. It reports possible secret leaks, like application tokens and cryptographic keys, in the source code and files contained in your project.

The analyzer wraps [Gitleaks](https://github.com/gitleaks/gitleaks) tool, and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast), [IaC](https://docs.gitlab.com/ee/user/application_security/iac_scanning), or [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## Guidance for rule changes

We are currently taking a cautious approach for accepting rule changes
as we work to document our criteria for accepting changes and improve the evaluation of rule changes.

For both new rules, as well as changes to existing rules, reviewers must evaluate
the BAP results to ensure there isn't a large increase in the time for BAP to process, which would indicate a slowdown. They should also ensure there isn't a difference in the number of findings and if there is, they should verify that is expected (such as increasing true positives, or decreasing false positives).

## Adding rules

The patterns for new rules must have a set prefix/suffix as well as having a defined length that can vary. An example pattern would be `acmepat-[0-9a-zA-Z_\-]{20,32}`

New rules can be added to the `./gitleaks.toml` file. So that the new rule doesn't generate excessive false positives, we recommend adding word boundries `\b` to the beginning of the regular expression and either `\b` or `['|\"|\n|\r|\s|\x60]` to the end. Choosing between ending the regular expression with `\b` or `['|\"|\n|\r|\s|\x60]` will depend on the complexity of the regular expression and the likelihood that using `\b` will result in excessive false positives. If you're unsure, test both and review the difference in the [BAP CI job](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/f33f86c7e4a2311086ea927558a285ee7951ccfd/.gitlab-ci.yml#L109).

To ensure the rule is working as expected, add an example token to `qa/fixtures/secrets/secrets.go` and then refresh the expected JSON using `analyzer-refresh-expected-json` from [analyzer scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md). Ensure the test token is included in the report. Also ensure that the token shows in the Findings section of the BAP Analysis Report, which runs as part of the CI Pipeline.

## Improving rules

Like adding new rules, we need to ensure rule changes are working as expected. Refresh the expected JSON using `analyzer-refresh-expected-json` from [analyzer scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md). Ensure the report is still correct.

### Tags

The `tags` field for a given rule is a gitleaks-native array of strings used for metadata and reporting purposes. We use several predefined tags, some for organizational purposes and others for special processing:

- `gitlab` - An organizational tag for grouping GitLab-specific rules for organizational purposes
- `gitlab_partner_token` - A organizational tag for grouping GitLab-partner verified rules
- `revocation_type` - A processing tag used for identifing rules linked to our [automatic response to leaked secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response.html) capability
- `gitlab_blocking` - A processing tag used to identify secrets which are processed as part of our [platform-wide experience](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/) in a blocking manner (i.e. pre-receive).

The `revocation_type` is a special tag that relies on the rules to have static IDs. These rule IDs must precisely map to the [token types](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/secret-revocation-service/-/blob/d14092c43574c0ed518a1e30cac6bcc17721eb99/srs/__init__.py#L31) as defined in our Secret Revocation Service to ensure revocation works as expected.

The `gitlab_blocking` is another special tag that is applied conditionally to patterns that will be used as part of our [platform-wide experience](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/). The patterns must have a set prefix/suffix that is at least 3 alphanumeric characters as well as having a set length. An example pattern would be `acmepat-[0-9a-zA-Z_\-]{20}` Not all patterns that meet this criteria will be tagged with `gitlab_blocking`. This is to ensure both performance and accuracy of the systems utilizing this tag. Due to the fact that evaluating a regex via code is complex, and that we want to be particular in regards to what gets the `gitlab_blocking` tag, this is not automated and final say in the usage of the tag will be done by the reviewers prior to merge.

### Syncing tags

Currently, syncing of tags is a manual process, and is only done for
syncing `gitlab_blocking` tags to the [gitleaks.toml file](https://gitlab.com/gitlab-org/gitlab/blob/master/gems/gitlab-secret_detection/lib/gitleaks.toml) of the Secret Push Protection gem. This will need to be done any time a `gitlab_blocking` tagged rule is added, updated, deleted, or untagged as `gitlab_blocking`. The Maintainer reviewer should ensure that the syncing happens, but the syncing may be done by someone else.

To start the syncing process, run the following from your secrets
repository directory:

`rake write_gitlab_blocking`

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.

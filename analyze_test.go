package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

func TestConfigPath(t *testing.T) {
	rootPath := "/root/path"
	tests := []struct {
		name string
		in   *ruleset.Config
		want string
	}{
		{
			name: "nil ruleset config",
			in:   nil,
			want: defaultPathGitleaksConfig,
		},
		{
			name: "passthrough without a matching target",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:  ruleset.PassthroughFile,
						Value: "gitleaks-config.toml",
					},
				},
			},
			want: defaultPathGitleaksConfig,
		},
		{
			name: "passthrough of type file (local)",
			in: &ruleset.Config{
				Passthrough: []ruleset.Passthrough{
					{
						Type:   ruleset.PassthroughFile,
						Target: "gitleaks.toml",
						Value:  "gitleaks-config.toml",
					},
				},
			},
			want: filepath.Join(rootPath, "gitleaks-config.toml"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path, err := configPath(rootPath, tt.in)

			if err != nil {
				t.Errorf("Expected a nil err, but got: %v", err)
			}

			assert.Equal(t, path, tt.want)
		})
	}
}

func TestConfigPathWithPassthroughRaw(t *testing.T) {
	rawGitleaksConfig := "raw gitleaks configuration"

	rulesetConfig := &ruleset.Config{
		Passthrough: []ruleset.Passthrough{
			{
				Type:   ruleset.PassthroughRaw,
				Target: "gitleaks.toml",
				Value:  rawGitleaksConfig,
			},
		},
	}
	path, err := configPath("/root/path", rulesetConfig)

	if err != nil {
		t.Errorf("Expected a nil err, but got: %v", err)
	}

	content, err := os.ReadFile(path)
	if err != nil {
		t.Fatalf("tried to read %s, but got %v", path, err)
	}

	assert.Equal(t, string(content), rawGitleaksConfig)
}
